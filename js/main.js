jQuery(function ($) {

    var $body = $('body');
    var windowWidth = Math.max($(window).width(), window.innerWidth);

    /*FitText*/
    $('h1').fitText(1.275, {minFontSize: '32px', maxFontSize: '40px'});
    $('.products-slider-box h2').fitText(1.275, {minFontSize: '38px', maxFontSize: '40px'});

    $('.section-text h2, .load-more-box h2, .consumers-service .text h2').fitText(1.6, {minFontSize: '30px', maxFontSize: '32px'});

    /*Responsive img*/
    if($('.responsimg').length){
        $('.responsimg').responsImg();
    }

    /*Parallax*/
    if ($('.parallax').length) {
        $('.parallax').parallax({
            limitX: 100,
            scalarX: 5,
            scalarY: 4
        });
    }

    /*Social*/
    if($('.share-box').length){
        $('.share-box').socialLikes({
            counters: true
        });
    }

    /*Tabs*/
    $('.tabs-box').easyResponsiveTabs({
        type: 'default',
        width: 'auto',
        fit: true,
        tabidentify: 'tabs'
    });

    /*17.11.2016*/
    function detectIE() {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf('MSIE ');
        var trident = ua.indexOf('Trident/');
        var edge = ua.indexOf('Edge/');
        if (msie > 0){
            $('html').addClass('ie');
        }
        else if (trident > 0){
            $('html').addClass('ie');
        }
        else if (edge > 0){
            $('html').addClass('ie');
        }
        return false;
    };
    detectIE();

    function videoBoxHeight(){
        $('.media-col').each(function(){
            var parentHeight = $(this).height();
            $(this).find('.video-box').attr('style', '');

            if(windowWidth <= 820){
                $(this).find('.video-box').css('height', parentHeight + 30);
            }
            else{
                $(this).find('.video-box').css('height', parentHeight + 2);
            }
        });
    }

    if($('.media-col .video-box').length && $('html').hasClass('ie')){
        videoBoxHeight();
    }

    /*Select2*/
    $(".select").select2({
        minimumResultsForSearch: Infinity
    });

    if(!$('html').hasClass('ie')){
        $('.select').next('.select2').find('.select2-selection').one('focus', select2Focus).on('blur', function () {
            $(this).one('focus', select2Focus)
        });

        function select2Focus() {
            $(this).closest('.select2').prev('select').select2('open');
        }
    }

    /*Header*/
    $('#header .search-form input').focus(function(){
        if(windowWidth > 920){
            $(this).addClass('in-focus');
        }
    });

    $('#header .search-form input').blur(function(){
        if(windowWidth > 920) {
            if (!$(this).val().length) {
                $(this).removeClass('in-focus');
            }
        }
    });

    /*NAV*/
    $('#nav > ul li').delegate('a:not(.active)', 'click', function (e) {
        var thisa = $(this);
        if(thisa.parent('li').hasClass('has-child')){
            e.preventDefault();
            e.stopPropagation();

            $('#all').addClass('dropdown-visible');

            thisa.parent('li').siblings().find('a').removeClass('active').next('.dropdown').hide();
            thisa.parent('li').siblings().removeClass('current');
            thisa.addClass('active').next('.dropdown').slideDown(200);
            thisa.parent('li').addClass('current');

            if(windowWidth > 920 && $('.nav-products .img-box').length){
                equalheight('.nav-products .img-box');
            }
        }
    });
    $('#nav > ul').children('li').delegate('a.active', 'click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).removeClass('active').next('.dropdown').slideUp(200);
        setTimeout(function(){
            $('#nav > ul > li').removeClass('current');
            $('#all').removeClass('dropdown-visible');
        }, 200);
    });

    $('#nav .dropdown').click(function(e){
        e.stopPropagation();
    });


    /*Control scroll*/
    var keys = {37: 1, 38: 1, 39: 1, 40: 1};
    function preventDefault(e) {
        e = e || window.event;
        if (e.preventDefault)
            e.preventDefault();
        e.returnValue = false;
    }

    function preventDefaultForScrollKeys(e) {
        if (keys[e.keyCode]) {
            preventDefault(e);
            return false;
        }
    }

    function disableScroll() {
        if (window.addEventListener) // older FF
            window.addEventListener('DOMMouseScroll', preventDefault, false);
        window.onwheel = preventDefault; // modern standard
        window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
        window.ontouchmove  = preventDefault; // mobile
        document.onkeydown  = preventDefaultForScrollKeys;
    }

    function enableScroll() {
        if (window.removeEventListener)
            window.removeEventListener('DOMMouseScroll', preventDefault, false);
        window.onmousewheel = document.onmousewheel = null;
        window.onwheel = null;
        window.ontouchmove = null;
        document.onkeydown = null;
    }

    $('#open-nav').click(function(e){
        e.stopPropagation();
        $('#nav, .overlay').addClass('visible');
        $('.overlay').addClass('mobile-nav');
        $('.open-search.active').trigger('click');
        $(window).scroll(function(){
            if($('#nav').hasClass('visible')){
                $('html, body').animate({
                    scrollTop: 0
                }, 0);
            }
        });

        $('#nav > ul li a').removeClass('active').next('.dropdown').hide();
        //disableScroll();
    });

    $('#close-nav').click(function(){
        $('#nav').removeClass('visible');
        setTimeout(function(){
            $('.overlay').removeClass('visible');
        }, 100);

        $('#nav > ul li a').removeClass('active').next('.dropdown').hide();
            $('#nav > ul > li').removeClass('current');
            $('#all').removeClass('dropdown-visible');
       // enableScroll();
    });

    function navPos(){
        if(windowWidth <= 920){
            $('#nav').insertAfter('#footer');
            $('#logo').insertAfter('#header .sub-box');
        }
        else{
            $('#nav').insertBefore('#open-nav');
            var logo = $('#header #logo').detach();
            $('#nav .logo-box').append(logo)
        }
    }
    navPos();
    /*_______*/

    /*Search*/
    $('.open-search').click(function(){
       if(!$(this).hasClass('active')){
           $(this).addClass('active').attr('title', 'Close search');
           $('#header .search-form').slideDown(150);
       }
        else{
           $(this).removeClass('active').attr('title', 'Open search');
           $('#header .search-form').slideUp(150);
       }
    });

    /*Main slider*/
    $("#main-slider").slick({
        adaptiveHeight: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: true,
        dots: true,
        speed: 200,
        arrows: false,
        cssEase: 'linear'
    });

    function mainSliderElementsPos(){
        if(windowWidth <= 920){
            $('#main-slider .item').each(function(){
                $(this).find('.description-col').insertBefore($(this).find('.box'));
            });
        }
        else{
            $('#main-slider .item').each(function(){
                $(this).find('.description-col').insertAfter($(this).find('.character-col'));
            });
        }
    }
    mainSliderElementsPos();



    /*Product slider*/
    var textAr = [];
    $('#product-slider').find('.item').each(function(){
        var $this = $(this);
        textAr.push({
            text: $this.data('text'),
            image: $this.data('preview')
        });
    });
    $('#product-slider').on('init', function(slick){
        $('#product-slider').find('.slick-arrow').each(function(){
            $('<span></span><img src="" alt="">').appendTo($(this));
        });
        var text = textAr[1].text;
        var preview = textAr[1].image;
        var textPrev = textAr[textAr.length-1].text;
        var previewPrev = textAr[textAr.length-1].image;

        $('#product-slider-box').find('.prod-next').find('.product-name').text(text);
        $('#product-slider-box').find('.prod-next').find('.preview').attr('src', preview);
        $('#product-slider-box').find('.prod-prev').find('.product-name').text(textPrev);
        $('#product-slider-box').find('.prod-prev').find('.preview').attr('src', previewPrev);
    });

    $("#product-slider").slick({
        adaptiveHeight: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: true,
        dots: true,
        speed: 200,
        nextArrow: '.prod-next',
        prevArrow: '.prod-prev',
        cssEase: 'linear'
    });

    $('#product-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
        if(currentSlide < nextSlide && nextSlide != textAr.length-1){
            $('#product-slider-box').find('.prod-next').find('.product-name').text(textAr[currentSlide+2].text);
            $('#product-slider-box').find('.prod-prev').find('.product-name').text(textAr[currentSlide].text);
            $('#product-slider-box').find('.prod-next').find('.preview').attr('src', textAr[currentSlide+2].image);
            $('#product-slider-box').find('.prod-prev').find('.preview').attr('src', textAr[currentSlide].image);
        }
        else if (currentSlide > nextSlide && nextSlide != 0){
            $('#product-slider-box').find('.prod-next').find('.product-name').text(textAr[currentSlide].text);
            $('#product-slider-box').find('.prod-prev').find('.product-name').text(textAr[currentSlide-2].text);
            $('#product-slider-box').find('.prod-next').find('.preview').attr('src', textAr[currentSlide].image);
            $('#product-slider-box').find('.prod-prev').find('.preview').attr('src', textAr[currentSlide-2].image);
        }
        else if (nextSlide == textAr.length-1) {
            $('#product-slider-box').find('.prod-next').find('.product-name').text(textAr[0].text);
            $('#product-slider-box').find('.prod-prev').find('.product-name').text(textAr[textAr.length-2].text);
            $('#product-slider-box').find('.prod-next').find('.preview').attr('src', textAr[0].image);
            $('#product-slider-box').find('.prod-prev').find('.preview').attr('src', textAr[textAr.length-2].image);
        }
        else if(nextSlide == 0) {
            $('#product-slider-box').find('.prod-next').find('.product-name').text(textAr[1].text);
            $('#product-slider-box').find('.prod-prev').find('.product-name').text(textAr[textAr.length-1].text);
            $('#product-slider-box').find('.prod-next').find('.preview').attr('src', textAr[1].image);
            $('#product-slider-box').find('.prod-prev').find('.preview').attr('src', textAr[textAr.length-1].image);
        }
    });
    /*____________*/

    /*Products slider*/
    $(".products-slider").slick({
        adaptiveHeight: true,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        dots: false,
        speed: 200,
        nextArrow: '.prod-next',
        prevArrow: '.prod-prev',
        cssEase: 'linear',
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 870,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    fade: true
                }
            }
        ]
    });

    /*Media*/
        /*Video*/
        $('.play-btn').click(function () {
            var parent = $(this).parents('.media-col');
            var videoId = parent.attr('data-video-id');
            if(parent.hasClass('vimeo-video')){
                $('<iframe src="https://player.vimeo.com/video/' +videoId + '?autoplay=1&loop=1&automute=0" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>').appendTo(parent.find('.video'));
            }
            else{
                $('<iframe src="https://www.youtube.com/embed/' + videoId + '?rel=0&autoplay=1&enablejsapi=1" width="100%" height="100%" frameborder="0" allowfullscreen></iframe>').appendTo(parent.find('.video'));
            }
            $(this).hide();
            parent.find('.poster').fadeOut(150);
            if ($('html').hasClass('touch')) {
                setTimeout(function(){
                    $('.ytp-button').trigger('click');
                }, 500)
            }
        });

        /*Position*/
        function mediaElementsPosition(){
            $('.media-box .row').each(function(){
                var content = $(this).find('.text-col.first-mobile').detach();
                if(windowWidth <= 820){
                    $(this).prepend(content);
                }
                else{
                    $(this).append(content);
                }
            });
        }
        if($('.media-box .text-col.first-mobile').length){
            mediaElementsPosition();
        }

    /*Equal height*/
    equalheight = function(container){
        var currentTallest = 0,
            currentRowStart = 0,
            rowDivs = new Array(),
            $el,
            topPosition = 0;
        $(container).each(function() {
            $el = $(this);
            $($el).height('auto')
            topPostion = $el.position().top;

            if (currentRowStart != topPostion) {
                for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                    rowDivs[currentDiv].height(currentTallest);
                }
                rowDivs.length = 0; // empty the array
                currentRowStart = topPostion;
                currentTallest = $el.height();
                rowDivs.push($el);
            } else {
                rowDivs.push($el);
                currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
            }
            for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                rowDivs[currentDiv].height(currentTallest);
            }
        });
    };
    /*_________*/

    /*Form*/

    /*Placeholder*/
    $('input, textarea').placeholder();

    if($('input[type="file"]').length){
        $('input[type="file"]').fileinput();
    }

    /*Mask*/
    if($('.num-mask').length){
        $(".num-mask").mask("99");
    }
    if($('.month-mask').length){
        $(".month-mask").mask("99");
    }
    if($('.year-mask').length){
        $(".year-mask").mask("9999");
    }
    if($('.phone-mask').length){
        $(".phone-mask").mask("+ (999) 99 999-99-99");
    }

    $('.open-hint').click(function(e){
        e.stopPropagation();
        if(!$(this).hasClass('active')){
            $('.open-hint').removeClass('active').next('.hint').hide();
            $(this).addClass('active').next('.hint').show();
            $('.fields-row, .field-col, .hint-box').attr("style", '');
            $(this).parents('.fields-row, .field-col, .hint-box').css("zIndex", 15);
        }
        else{
            $(this).removeClass('active').next('.hint').hide();
            $('.fields-row, .field-col, .hint-box').attr("style", '');
        }
    });

    $('.hint').click(function(e){
        e.stopPropagation();
    });

    $('.copy-child').click(function(){
        var copyHtml = $('.copy-box').html();
        $(this).parents('.fields-row').before(copyHtml);

        $(".num-mask").mask("99");
        $(".month-mask").mask("99");
        $(".year-mask").mask("9999");

    });
    /*______________*/


    /*Load more ajax*/

    var loading = false;

    $body.on('click', ".load-more", function(e){
        e.preventDefault();
        if (loading) {
            alert('Please wait');
        }
        else{
            var url = $(this).attr('href');
            if($(this).hasClass('load-news')){
                loadNews(url, true);
            }
            else if($(this).hasClass('load-recipes')){
                loadRecipes(url, true);
            }
            else if($(this).hasClass('load-colouring')){
                loadColouring(url, true);
            }
        }
    });

    function loadNews(url) {
        $.ajax({
            url: url,
            dataType: 'html',
            beforeSend: function () {
                loading = true;
            },
            success: function(data) {
                /*Text*/
                var content = $(data).filter('.articles-list').html();

                if($(data).filter('.more-news').length){
                    var newHref = $(data).filter('.more-news').attr('href');
                    $('.load-more').attr('href', newHref);
                }
                else{
                    $('.load-more-box').hide();
                }

                $('.articles-list').append(content);

                /*Animation*/
                setTimeout(function(){
                    $('.articles-list li.hidden').removeClass('hidden');
                }, 50);

                loading = false;
            },
            error: function () {
                loading = false;
                alert('Page not found!');
            }
        });
    }

    function loadRecipes(url) {
        $.ajax({
            url: url,
            dataType: 'html',
            beforeSend: function () {
                loading = true;
            },
            success: function(data) {
                /*Text*/
                var content = $(data).filter('.recipes-list').html();

                if($(data).filter('.more-recipes').length){
                    var newHref = $(data).filter('.more-recipes').attr('href');
                    $('.load-more').attr('href', newHref);
                }
                else{
                    $('.load-more-box').hide();
                }

                $('.recipes-list').append(content);

                $('.responsimg').responsImg();

                /*Animation*/
                $('.recipes-list .hidden').removeClass('hidden');

                loading = false;
            },
            error: function () {
                loading = false;
                alert('Page not found!');
            }
        });
    }

    function loadColouring(url) {
        $.ajax({
            url: url,
            dataType: 'html',
            beforeSend: function () {
                loading = true;
            },
            success: function(data) {
                /*Text*/
                var content = $(data).filter('.colouring-list').html();

                $('.load-more-box').hide();

                $('.colouring-list').append(content);

                /*Animation*/
                setTimeout(function(){
                    $('.colouring-list .hidden').removeClass('hidden');
                }, 5);

                $lg.data('lightGallery').destroy(true);

                $lg.lightGallery({
                    hash: false,
                    selector: '.item'
                });

                $('.colouring-list .item').click(function(){
                    $('body').removeClass('color1').removeClass('color2').removeClass('color3').removeClass('color4');
                    if($(this).parents('.item-box').hasClass('color1')){
                        $('body').addClass('color1');
                    }
                    else if($(this).parents('.item-box').hasClass('color2')){
                        $('body').addClass('color2');
                    }
                    else if($(this).parents('.item-box').hasClass('color3')){
                        $('body').addClass('color3');
                    }
                    else if($(this).parents('.item-box').hasClass('color4')){
                        $('body').addClass('color4');
                    }
                });

                $lg.on('onCloseAfter.lg', function(){
                    $('body').removeClass('color1').removeClass('color2').removeClass('color3').removeClass('color4');
                });

                /*Colouring action*/
                $('.no-touch .colouring-list .item').hover(function(){
                    $(this).parents('.item-box').addClass('hover');
                }, function(){
                    $(this).parents('.item-box').removeClass('hover');
                });

                loading = false;
            },
            error: function () {
                loading = false;
                alert('Page not found!');
            }
        });
    }

    /*Recipes slider*/
    $(".recipe-slider").slick({
        adaptiveHeight: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: true,
        dots: false,
        fade: true,
        speed: 200,
        cssEase: 'linear'
    });

    $('.recipe-slider .slick-prev').text('<');
    $('.recipe-slider .slick-next').text('>');

    /*Colouring action*/
    $('.no-touch .colouring-list .item').hover(function(){
        $(this).parents('.item-box').addClass('hover');
    }, function(){
        $(this).parents('.item-box').removeClass('hover');
    });


    $('.colouring-list .item').click(function(){
        $('body').removeClass('color1').removeClass('color2').removeClass('color3').removeClass('color4');
        if($(this).parents('.item-box').hasClass('color1')){
            $('body').addClass('color1');
        }
        else if($(this).parents('.item-box').hasClass('color2')){
            $('body').addClass('color2');
        }
        else if($(this).parents('.item-box').hasClass('color3')){
            $('body').addClass('color3');
        }
        else if($(this).parents('.item-box').hasClass('color4')){
            $('body').addClass('color4');
        }
    });

    var $lg = $('.colouring-list');

    if($lg.length){
        $lg.lightGallery({
            hash: false,
            selector: '.item'
        });
    }

    $lg.on('onCloseAfter.lg', function(){
        $('body').removeClass('color1').removeClass('color2').removeClass('color3').removeClass('color4');
    });


    /*Sub actions*/
    $('.overlay').click(function(){
        $('#close-nav').trigger('click');
    });

    $('#all, #footer').click(function(){
        $('#nav > ul li a').removeClass('active').next('.dropdown').slideUp(200);
        setTimeout(function(){
            $('#nav > ul > li').removeClass('current');
            $('#all').removeClass('dropdown-visible');
        }, 200);
        $('.open-hint.active').trigger('click');
    });

    $(document).keyup(function(e) {
        if(e.keyCode == 27){
            $('#close-nav, .open-search.active').trigger('click');
        }
    });

    /*Footer*/
    function stickyFooter(){
        var fHeight = $('#footer').height();
        $('#footer').css('marginTop', - fHeight);
        $('#indent').css('paddingBottom', fHeight);
    }
    /*______*/


    $(window).load(function(){
        if(windowWidth > 870){
            equalheight('.nav-products .img-box, .products-slider .img-box');
        }
        stickyFooter();
        $(window).trigger('resize');
    });

    $(window).resize(function(){
        windowWidth = Math.max( $(window).width(), window.innerWidth);
        mainSliderElementsPos();
        navPos();
        stickyFooter();
        if(windowWidth > 870){
            equalheight('.products-slider .img-box');
        }
        waitForFinalEvent(function(){
            if(windowWidth > 870){
                equalheight('.nav-products .img-box');
            }
            if($('.media-box .text-col.first-mobile').length){
                mediaElementsPosition();
            }
            if($('.media-col .video-box').length && $('html').hasClass('ie')){
                videoBoxHeight();
            }
        }, 50);
    });

    $(window).on('orientationchange', function(){
        if(windowWidth > 920){
            equalheight('.nav-products .img-box, .products-slider .img-box');
        }
        if($('.media-box .text-col.first-mobile').length){
            mediaElementsPosition();
        }
        if($('.media-col .video-box').length && $('html').hasClass('ie')){
            videoBoxHeight();
        }
        navPos();
        mainSliderElementsPos();
        stickyFooter();
    });

    $(window).scroll(function(){

    });

});

var waitForFinalEvent = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();



