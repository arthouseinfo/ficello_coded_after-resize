(function ($) {
    $.fn.fileinput = function (o) {

        o = $.extend({
            title: 'Browse...'
        }, o || {});

        this.each(function (i, elem) {

            var $elem = $(elem);
            if (typeof $elem.attr('data-fileinput-disabled') != 'undefined') {
                return;
            }

            var buttonWord = o.title;
            if (typeof $elem.attr('title') != 'undefined') {
                buttonWord = $elem.attr('title');
            }

            var className = '';
            if (!!$elem.attr('class')) {
                className = ' ' + $elem.attr('class');
            }

            $elem.wrap('<span class="fileinput btn btn-default' + className + '"></span>');
            $elem.closest('.fileinput').prepend($('<span></span>').html(buttonWord));
            $elem.closest('.fileinput').wrap($('<span class="fileinput-wrapper"></span>'));

        }).promise().done(function () {

            // After we have found all of the file inputs let's apply a listener for tracking the mouse movement.

            var $body = $('body');

            // change
            $body.on('change', '.fileinput input[type=file]', function () {

                var fileName = $(this).val();

                // Remove any previous file names
                $(this).closest('.fileinput-wrapper').find('.fileinput-name').remove();
                if (!!$(this).prop('files') && $(this).prop('files').length > 1) {
                    fileName = $(this)[0].files.length + ' files';
                } else {
                    fileName = fileName.substring(fileName.lastIndexOf('\\') + 1, fileName.length);
                }

                // Don't try to show the name if there is none
                if (!fileName) {
                    return;
                }

                // Print the fileName aside (right after the the button)
                $(this).closest('.fileinput-wrapper').append('<span class="fileinput-name">' + fileName + '<button type="button" class="fileinput-clear close">&times;</button></span>');
            });

            // clear
            $body.on('click', '.fileinput-clear', function (e) {
                e.preventDefault();
                var wrapper = $(this).closest('.fileinput-wrapper').find('.fileinput');
                var input = wrapper.find('input');
                var inputClone = input.clone(true);
                input.after(inputClone);
                input.remove();
                $(this).closest('.fileinput-name').remove();
            });

        });

    };

})(jQuery);
